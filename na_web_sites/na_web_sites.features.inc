<?php
/**
 * @file
 * na_web_sites.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function na_web_sites_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function na_web_sites_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function na_web_sites_image_default_styles() {
  $styles = array();

  // Exported image style: tiny40.
  $styles['tiny40'] = array(
    'label' => 'tiny40',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 40,
          'height' => 40,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function na_web_sites_node_info() {
  $items = array(
    'web_links' => array(
      'name' => t('Web Links'),
      'base' => 'node_content',
      'description' => t('Web links for Narcotics Anonymous sites.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
