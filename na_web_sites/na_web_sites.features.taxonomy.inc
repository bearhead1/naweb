<?php
/**
 * @file
 * na_web_sites.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function na_web_sites_taxonomy_default_vocabularies() {
  return array(
    'web_links' => array(
      'name' => 'Web Links',
      'machine_name' => 'web_links',
      'description' => 'Links to Narcotics Anonymous Sites',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
